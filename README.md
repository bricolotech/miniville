# La miniville connectée

L'idée est de construire les bases d'une miniville connectée. Voies, décords, signalisations, véhicules. Ces éléments seront controlables à distance mais capable de fonctionner en autonomie.

## Durée du projet

C'est un projet prévue sur une durée d'un an à raison d'un atelier de deux heures par semaine appellé bricolotech. 

[Pour la fête de la science nous avons décidé de faire une sorte de demostration afin d'illustrer l'idée d'une smartcity connectée. Il s'agit d'un travail evenementiel](https://framagit.org/bricolotech/miniville/wikis/home)

## D'où vient ce projet?

Je m'interesse à de nombreux domaines tel que la modélisation 3D, l'électricité, les objets connectés, le système, les réseaux, le développement logiciel. J'ai pensé à ce projet afin de pouvoir réunir tous ces univers. Lorsque j'en ai parlé autour de moi, j'ai trouvé de l'intérêt pour ce projet et de nombreuses idées(parfois contradictoires). Miniville à la bricolotech sera une implémentation particulière de miniville mais à ce jour d'autres implémentations sont en cours. Ce projet est public sous licence GPL V3.

## BOM ou de quoi a-t-on besoin

BOM signifie Bill Of Material. Cela permet d'évaluer le cout matériel d'un projet. Je souhaite mettre au point cette liste avec les acteurs de la miniville Bricolotech. Cette liste est indépendante des autres implémentation. Voici le minimum vital :

* Imprimante 3D + PLA (généreusement prété par G.C. je ne sais pas si je peux le citer) : 200€
* CNC + bois de maquete [ optionnel ] : 300€
* Microcontrolleurs wifi type ESP8266 ou ESP32 (1 par véhicule) : 3 à 10€ par véhicule selon le fournisseur.
* dev board centrale type raspi mais idéalement plus libre que le soc broadcom. : 15 à 100€ selon la board et le fournisseur.
* Minimicrocontroleurs pour les feux/aiguillage/eclairage type attiny(1 par feux me semble le plus simple et rapide à mettre en place) : 1 à 4€ selon fournisseur.
* drivers de moteurs type ld293d (1 par véhicule, à moins que l'on n'utilise des servo-moteurs...) : 0,5 et 1,2 € par driver selon le fournisseur.
* moteur ou servo( 2 par véhicule)
* plaque d'essai
* fil
* fer à souder [ optionnel ]
* fil de soudure [ optionnel ]
* pile ou batterie 
* minicamera pour streaming video(un pour chaque véhicule) [ optionnel ]
* webcam controlé depuis la devboard [ optionnel ]
